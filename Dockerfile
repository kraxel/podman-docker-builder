#FROM quay.io/centos/centos:stream8
FROM quay.io/almalinuxorg/8-base

ENV SUMMARY="podman-docker-builder" \
    DESCRIPTION="build docker images using podman"

LABEL maintainer="Gerd Hoffmann <kraxel@redhat.com>" \
      summary="${SUMMARY}" \
      description="${DESCRIPTION}"

USER root

COPY configure-* /usr/local/bin
RUN /usr/local/bin/configure-mirror && \
    /usr/local/bin/configure-podman
