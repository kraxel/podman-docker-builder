# podman docker image builder

This image can build container images from a Dockerfile.
It uses centos 8 with podman.

## using the image

Simply use registry.gitlab.com/kraxel/podman-docker-builder:latest
instead of docker:latest in your .gitlab-ci.yml file.  dind
(docker-in-docker) is not needed and can be dropped.  All your
docker commands are automatically redirected to podman, so there
is no need to change them all.

## compatibility

The usual build workflow with pull / build / tag / push docker
commands should work just fine.  There isn't a docker daemon
running though.

## template

There is a template in this repo which you can use via include in
.gitlab-ci.yml:

```
include:
  - project: 'kraxel/podman-docker-builder'
    file: 'gitlab-ci-template.yml'
```
